
package SQL_posgressql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author ronal
 */
public class sql {
       Connection conn = null;
    String url = "jdbc:postgresql://localhost/ProgramaClase23";
    String user = "postgres";
    String pass = "DesmondGc";
    
    PreparedStatement ps=null;
    ResultSet rs=null;

    public void conectar() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            ps = conn.prepareStatement("Selec* FROM Materia WHERE id=?");
            JOptionPane.showMessageDialog(null, "Conexion exitosa", "conexion", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "conexion fallida" + e, "conexion", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void cerrar() {
        try {

            conn.close();
            JOptionPane.showMessageDialog(null, "desconexion exitosa", "desconexion", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "desconexion fallida" + e, "desconexion", JOptionPane.ERROR_MESSAGE);
        }
    }
}
